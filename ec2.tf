terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.1.9"
}

provider "aws" {
  region     = "ap-south-1"
  access_key = "AKIARORXMYFHSDDIDYNQ"
  secret_key = "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"
}

resource "aws_iam_instance_profile" "ec2_instance_profile" {
  name = "ec2_instance_profile"
  role = "ec2_to_codedeploy"
}
resource "aws_instance" "app_server" {
  ami                  = "ami-006d3995d3a6b963b"
  instance_type        = "t2.micro"
  iam_instance_profile = "ec2_instance_profile"
  security_groups      = ["default"]
  user_data            = <<EOF
#!/bin/bash
sudo apt-get -y update
sudo apt -y update

# INSTALL CODEDEPLOY AGENT
sudo apt install -y ruby-full
sudo apt install -y wget
wget https://aws-codedeploy-ap-south-1.s3.ap-south-1.amazonaws.com/latest/install
sudo chmod +x ./install
sudo ./install auto > /tmp/logfile
sudo service codedeploy-agent status

# INSTALL NODEJS - ONLY FOR UBUNTU
sudo apt install nodejs -y

# INSTALL NPM & PM2 - ONLY FOR UBUNTU
sudo apt install npm -y
sudo npm install pm2@latest -g
EOF
  tags = {
    Name = var.instance_name
  }
}

resource "aws_codedeploy_app" "codedeploy_app" {
  name = "codedeploy_app"
}

resource "aws_sns_topic" "sns_topic" {
  name = "sns-topic"
}

resource "aws_codedeploy_deployment_group" "codedeploy_group" {
  app_name              = aws_codedeploy_app.codedeploy_app.name
  deployment_group_name = "codedeploy_group"
  service_role_arn      = "arn:aws:iam::099974168911:role/codedeploy_to_ec2"

  ec2_tag_set {
    ec2_tag_filter {
      key   = "Name"
      type  = "KEY_AND_VALUE"
      value = var.instance_name
    }
  }

  trigger_configuration {
    trigger_events     = ["DeploymentFailure"]
    trigger_name       = "test-trigger"
    trigger_target_arn = aws_sns_topic.sns_topic.arn
  }

  auto_rollback_configuration {
    enabled = true
    events  = ["DEPLOYMENT_FAILURE"]
  }

  alarm_configuration {
    alarms  = ["my-alarm-name"]
    enabled = true
  }
}
